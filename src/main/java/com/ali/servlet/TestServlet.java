package com.ali.servlet;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/test/hello")
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("TestServlet.req.method：" + req.getMethod());
        System.out.println("TestServlet.req.url：" + req.getRequestURL());
        System.out.println("TestServlet.req.protocol：" + req.getProtocol());
        ServletOutputStream servletOutputStream = resp.getOutputStream();
        servletOutputStream.write("hello world".getBytes());
    }
}
