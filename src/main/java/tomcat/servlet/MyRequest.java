package tomcat.servlet;

import java.net.Socket;

/**
 * 自定义请求参数
 */
public class MyRequest extends AbstractHttpServletRequest {
    private final String method;
    private final String url;
    private final String protocol;
    private final Socket socket;

    public MyRequest(String method, String url, String protocol, Socket socket) {
        this.method = method;
        this.url = url;
        this.protocol = protocol;
        this.socket = socket;
    }

    @Override
    public String getMethod() {
        return method;
    }

    @Override
    public StringBuffer getRequestURL() {
        return new StringBuffer(url);
    }

    @Override
    public String getProtocol() {
        return protocol;
    }

    public Socket getSocket() {
        return socket;
    }
}
