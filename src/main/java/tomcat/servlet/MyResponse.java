package tomcat.servlet;

import tomcat.common.CharType;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义返回参数
 */
public class MyResponse extends AbstractHttpServletResponse {
    private int status = 200;
    private String message = "OK";
    private final MyRequest myRequest;
    private final OutputStream socketOutputStream;
    private final Map<String, String> headers = new HashMap<>();
    private final MyServletOutputStream myServletOutputStream = new MyServletOutputStream();

    public MyResponse(MyRequest myRequest) {
        this.myRequest = myRequest;
        try {
            this.socketOutputStream = myRequest.getSocket().getOutputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setStatus(int i, String s) {
        status = i;
        message = s;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void addHeader(String s, String s1) {
        headers.put(s, s1);
    }

    @Override
    public MyServletOutputStream getOutputStream() throws IOException {
        return myServletOutputStream;
    }

    /**
     * 发送响应参数
     */
    public void complete() {
        sendResponseLine();
        sendResponseHeader();
        sendResponseBody();
    }

    /**
     * 发送响应行
     */
    private void sendResponseLine() {
        try {
            socketOutputStream.write(myRequest.getProtocol().getBytes());
            socketOutputStream.write(CharType.SP);
            socketOutputStream.write(status);
            socketOutputStream.write(CharType.SP);
            socketOutputStream.write(message.getBytes());
            socketOutputStream.write(CharType.CR);
            socketOutputStream.write(CharType.LF);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 发送响应头
     */
    private void sendResponseHeader() {
        try {
            if (!headers.containsKey("Content-Length")) {
                addHeader("Content-Length", String.valueOf(getOutputStream().getNum()));
            }
            if (!headers.containsKey("Content-Length")) {
                addHeader("Content-Type", "text/html;charset=utf-8");
            }
            for (String key : headers.keySet()) {
                socketOutputStream.write(key.getBytes());
                socketOutputStream.write(":".getBytes());
                socketOutputStream.write(headers.get(key).getBytes());
                socketOutputStream.write(CharType.CR);
                socketOutputStream.write(CharType.LF);
            }
            socketOutputStream.write(CharType.CR);
            socketOutputStream.write(CharType.LF);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 发送响应体
     */
    private void sendResponseBody() {
        try {
            socketOutputStream.write(getOutputStream().getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
