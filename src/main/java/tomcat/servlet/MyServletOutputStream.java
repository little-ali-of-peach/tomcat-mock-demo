package tomcat.servlet;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;

/**
 * 自定义Servlet输出流
 */
public class MyServletOutputStream extends ServletOutputStream {
    private final byte[] bytes = new byte[1024];
    private int num = 0;

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setWriteListener(WriteListener writeListener) {
    }

    /**
     * 暂存响应体
     */
    @Override
    public void write(int b) {
        bytes[num] = (byte) b;
        num++;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public int getNum() {
        return num;
    }
}
