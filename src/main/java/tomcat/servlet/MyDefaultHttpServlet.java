package tomcat.servlet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义默认Servlet
 */
public class MyDefaultHttpServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("未匹配到Servlet，进入默认Servlet");
        System.out.println("MyDefaultServlet.req.method：" + req.getMethod());
        System.out.println("MyDefaultServlet.req.url：" + req.getRequestURL());
        System.out.println("MyDefaultServlet.req.protocol：" + req.getProtocol());
        ServletOutputStream servletOutputStream = resp.getOutputStream();
        servletOutputStream.write("error".getBytes());
    }
}
