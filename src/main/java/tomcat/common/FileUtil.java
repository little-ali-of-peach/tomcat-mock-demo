package tomcat.common;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件处理工具类
 */
public class FileUtil {
    /**
     * 获取所有文件
     */
    public static List<File> getAllFile(File rootFile) {
        List<File> result = new ArrayList<>();
        File[] files = rootFile.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    result.addAll(getAllFile(file));
                } else {
                    result.add(file);
                }
            }
        }
        return result;
    }
}
