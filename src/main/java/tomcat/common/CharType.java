package tomcat.common;

public class CharType {
    public static final char SP = ' ';
    public static final char CR = '\r';
    public static final char LF = '\n';
}
